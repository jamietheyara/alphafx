﻿using AlphaFx.TextFileProcessor.Utilities;
using System;

namespace AlphaFx.TextFileProcessor.Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var filePath = args[0];

            var fileHandler = new FileHandler();
            var degreeOfParallelism = Environment.ProcessorCount * 2;

            var fileProcessor = new FileProcessor(fileHandler, degreeOfParallelism);

            var topEntries = fileProcessor.ProcessTextFile(filePath);

            Console.WriteLine(string.Join(Environment.NewLine, topEntries));

            Console.ReadLine();
        }
    }
}
