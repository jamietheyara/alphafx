﻿using System.Collections.Concurrent;
using System.Linq;

namespace AlphaFx.TextFileProcessor.Extensions
{
    public static class ConcurrentDictionaryExetensions
    {
        public static string[] GetTopEntries(this ConcurrentDictionary<string, int> wordDictionary, int entries = 10)
        {
            var orderedDictionary = wordDictionary.OrderByDescending(wordValuePair => wordValuePair.Value);

            var formattedArray = orderedDictionary.Select(wordValuePair => $"'{wordValuePair.Key}' occurred {wordValuePair.Value} times").Take(entries);

            return formattedArray.ToArray();
        }
    }
}
