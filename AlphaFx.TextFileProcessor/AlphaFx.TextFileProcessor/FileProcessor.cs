﻿using AlphaFx.TextFileProcessor.Extensions;
using AlphaFx.TextFileProcessor.Utilities;
using System.Collections.Concurrent;
using System.Linq;

namespace AlphaFx.TextFileProcessor
{
    public class FileProcessor
    {
        private readonly IFileHandler _fileHandler;
        private readonly ILineSplitter _lineSplitter;
        private int _degreeOfParallelism;

        public FileProcessor(IFileHandler fileHandler, int degreeOfParallelism)
        {
            _fileHandler = fileHandler;
            _lineSplitter = new LineSplitter();
            _degreeOfParallelism = degreeOfParallelism;
        }


        public string[] ProcessTextFile(string filePath)
        {
            var wordDictionary = new ConcurrentDictionary<string, int>();

            _fileHandler.ReadLines(filePath)
                .AsParallel()
                .WithDegreeOfParallelism(_degreeOfParallelism).ForAll(textLine =>
                {
                    _lineSplitter.SplitLine(wordDictionary, textLine);

                });

            return wordDictionary.GetTopEntries();
        }
    }
}
