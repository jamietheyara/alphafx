﻿using AlphaFx.TextFileProcessor.Utilities;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace AlphaFx.TextFileProcessor.Tests
{
    [TestFixture]
    public class FileProcessorTests
    {

        private Mock<IFileHandler> _mockFileHandler;

        private readonly string _textLine1 = "This is the first line of text, no surprises here";
        private readonly string _textLine2 = "Well this is the second line of text, the second line of text";
        private readonly string _textLine3 = "We are back for the thirs line this time, third or 3rd, either is fine.";
        private readonly string _textLine4 = "The penultimate line, the magical number 4, actually 3 is the magic number, this 4th line is average";
        private readonly string _textLine5 = "Last but not least, the 5th (fifth) line is finally here. Congratulations";

        [SetUp]
        public void Setup()
        {
            _mockFileHandler = new Mock<IFileHandler>();
        }

        [Test]
        public void Calling_ProcessFile_ProcessesFile()
        {
            var fileProcessor = new FileProcessor(_mockFileHandler.Object, 16);

            _mockFileHandler.Setup(fh => fh.ReadLines(It.IsAny<string>())).Returns(new List<string>() { _textLine1, _textLine2, _textLine3, _textLine4, _textLine5 });

            var orderedEntries = fileProcessor.ProcessTextFile("aTextFile.txt");

            Assert.That(orderedEntries[0], Does.Contain("the"));
            Assert.That(orderedEntries[0], Does.Contain("7"));
        }

    }
}
