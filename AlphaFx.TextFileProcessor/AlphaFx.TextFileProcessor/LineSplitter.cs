﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace AlphaFx.TextFileProcessor
{
    public class LineSplitter : ILineSplitter
    {
        public void SplitLine(ConcurrentDictionary<string, int> wordDictionary, string lineToSplit, bool removeNonAlphaNumerics = false)
        {
            var splitLine = lineToSplit.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            foreach (var word in splitLine)
            {
                if (removeNonAlphaNumerics && !word.All(char.IsLetterOrDigit))
                {
                    continue;
                }

                wordDictionary.AddOrUpdate(word, 1, (updateWord, value) => value + 1);
            }
        }
    }
}
