﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace AlphaFx.TextFileProcessor
{
    public interface ILineSplitter
    {
        void SplitLine(ConcurrentDictionary<string, int> wordDictionary, string lineToSplit, bool removeNonAlphaNumerics = false);
    }
}
