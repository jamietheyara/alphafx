﻿using NUnit.Framework;
using System.Collections.Concurrent;

namespace AlphaFx.TextFileProcessor.Tests
{
    [TestFixture]
    public class LineSplitterTests
    {
        [TestCase]
        public void Calling_SplitLine_UpdatesDictionary()
        {
            var concurrentDictionary = new ConcurrentDictionary<string, int>();

            var lineSplitter = new LineSplitter();

            var lineToSplit = "This is a test for Alpha FX, i should probably duplicate some words in order to show what i need to achieve, so yeah, this is a test for Alpha FX";

            lineSplitter.SplitLine(concurrentDictionary, lineToSplit);

            Assert.That(concurrentDictionary.ContainsKey("is"));

            concurrentDictionary.TryGetValue("is", out int isValue);

            Assert.That(isValue, Is.EqualTo(2));
        }
    }
}
