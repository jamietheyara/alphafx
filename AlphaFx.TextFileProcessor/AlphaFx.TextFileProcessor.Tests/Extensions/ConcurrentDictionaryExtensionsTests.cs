﻿using AlphaFx.TextFileProcessor.Extensions;
using NUnit.Framework;
using System.Collections.Concurrent;

namespace AlphaFx.TextFileProcessor.Tests.Extensions
{
    [TestFixture]
    public class ConcurrentDictionaryExtensionsTests
    {
        [TestCase(10)]
        public void Calling_GetTopEntries_ReturnsTopEntries(int numberOfEntries)
        {
            var concurrentDictionary = new ConcurrentDictionary<string, int>();

            concurrentDictionary.TryAdd("hello", 12);

            var orderedItem = concurrentDictionary.GetTopEntries();

            Assert.That(orderedItem[0], Does.Contain("hello"));
        }
    }
}
