﻿using System.Collections.Generic;
using System.IO;

namespace AlphaFx.TextFileProcessor.Utilities
{
    public class FileHandler : IFileHandler
    {
        public IEnumerable<string> ReadLines(string filePath)
        {
            return File.ReadLines(filePath);
        }
    }
}
