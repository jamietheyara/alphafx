﻿using System.Collections.Generic;

namespace AlphaFx.TextFileProcessor.Utilities
{
    public interface IFileHandler
    {
        IEnumerable<string> ReadLines(string filePath);
    }
}
